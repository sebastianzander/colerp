/*
* converter.h
* Color model conversion functions.
*
* Author:   Sebastian Zander
* Created:  2019-03-20
*/

#ifndef _CONVERTER_H
#define _CONVERTER_H

#include "types.h"

int rgbToHex(rgb_t rgb, int* out);
int rgbToHex3f(float r, float g, float b, int* out);
int rgbToHsl(rgb_t rgb, hsl_t* out);
int rgbToHsl3f(float r, float g, float b, hsl_t* out);
int rgbToHsv(rgb_t rgb, hsv_t* out);
int rgbToHsv3f(float r, float g, float b, hsv_t* out);
int rgbToHslsv(rgb_t rgb, hslsv_t* out);
int rgbToHslsv3f(float r, float g, float b, hslsv_t* out);
int rgbToHwb(rgb_t rgb, hwb_t* out);
int rgbToHwb3f(float r, float g, float b, hwb_t* out);
int rgbToCmyk(rgb_t rgb, cmyk_t* out);
int rgbToCmyk3f(float r, float g, float b, cmyk_t* out);

int hexToRgb(int hex, rgb_t* out);
int hexToRgb3by(byte r, byte g, byte b, rgb_t* out);

int hslToRgb(hsl_t hsl, rgb_t* out);
int hslToRgb3f(float h, float s, float l, rgb_t* out);

int hsvToRgb(hsv_t hsv, rgb_t* out);
int hsvToRgb3f(float h, float s, float v, rgb_t* out);

int hwbToRgb(hwb_t hsl, rgb_t* out);
int hwbToRgb3f(float h, float w, float b, rgb_t* out);

int cmykToRgb(cmyk_t cmyk, rgb_t* out);
int cmykToRgb4f(float c, float m, float y, float k, rgb_t* out);

#endif