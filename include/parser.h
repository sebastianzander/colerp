/*
* parser.h
* Color and component value parsing functions.
*
* Author:   Sebastian Zander
* Created:  2019-03-20
*/

#ifndef _PARSER_H
#define _PARSER_H

#include "types.h"

float parseComponentValue(const char* str);
int parseColorString(const char* str, hsl_t* hsl_out, inputcolor_t* out);
int formatOutputColor(rgb_t rgb, const char* output_format_string, char* output_buffer, int buffer_length);

#endif