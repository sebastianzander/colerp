/*
* utilities.h
* Utility and helper functions.
*
* Author:   Sebastian Zander
* Created:  2019-03-20
*/

#ifndef _UTILITIES_H
#define _UTILITIES_H

#define M_PI 3.14159265358979323846

float fmodx(float x, float y);
float evalsquare(float x);
float evallinear(float x);
float evalquadratic(float x);
float evalcubic(float x);
float evalquartic(float x);
float evalsinusoidal(float x);
float clamp(float value, float min, float max);
float mixf(float source, float target, float mean, float span);
float mix(float source, float target, float factor);

#endif