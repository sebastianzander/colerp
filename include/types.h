/*
* types.h
* Type definitions.
*
* Author:   Sebastian Zander
* Created:  2019-03-20
*/

#ifndef _TYPES_H
#define _TYPES_H

#include "defines.h"

typedef unsigned char byte;

typedef struct {
    const char* short_form;
    const char* long_form;
    const char* description;
} optionentry_t;

typedef struct {
    const char* command;
    const char* description;
} exampleentry_t;

typedef struct {
    float r;
    float g;
    float b;
} rgb_t;

typedef struct {
    float h;
    float s;
    float l;
} hsl_t;

typedef struct {
    float h;
    float s;
    float v;
} hsv_t;

typedef struct {
    float h;
    float s;
    float l;
    float sv;
    float v;
} hslsv_t;

typedef struct {
    float h;
    float w;
    float b;
} hwb_t;

typedef struct {
    float c;
    float m;
    float y;
    float k;
} cmyk_t;

typedef enum {
    CT_NONE, 
    CT_RED, 
    CT_GREEN, 
    CT_BLUE, 
    CT_HUE, 
    CT_SATURATION_HSL, 
    CT_SATURATION_HSV, 
    CT_LIGHTNESS, 
    CT_VALUE, 
    CT_CYAN, 
    CT_MAGENTA, 
    CT_YELLOW, 
    CT_BLACK
} EComponentType;

typedef enum {
    NF_INTEGER,
    NF_INTEGER_HEX, 
    NF_FLOAT, 
    NF_PERCENTAGE, 
    NF_DEGREES
} ENumberFormat;

typedef struct component_s {
    float value;
    EComponentType component_type;
    ENumberFormat number_format;
    int leading_zeroes;
    int integer_digits;
    int decimal_digits;
} component_t;

typedef struct colorformat_s {
    int num_components;
    component_t* components;
} colorformat_t;

typedef struct inputcolor_s {
    rgb_t rgb;
    hsl_t hsl;
    const char* color_string;
    int num_components;
    component_t* components;
} inputcolor_t;

typedef struct inputcolorarray_s {
    int num_actual_elements;
    int num_elements;
    inputcolor_t* elements;
} inputcolorarray_t;

typedef float (*evalfun)(float);

#endif
