/*
* defines.h
* Definitions and constants.
*
* Author:   Sebastian Zander
* Created:  2019-03-20
*/

#ifndef _DEFINES_H
#define _DEFINES_H

#ifndef NULL
#define NULL 0
#endif

#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_PATCH 0
#define VERSION_BUILD 7
#define VERSION_COMPILED "Mar 22 2019"

#define MAX_TEXT_WIDTH 135

#endif