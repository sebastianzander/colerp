/*
* converter.c
* Color model conversion functions.
*
* Author:   Sebastian Zander
* Created:  2019-03-20
*/

#include <math.h>

#include "converter.h"
#include "utilities.h"

int rgbToHex(rgb_t rgb, int* out)
{
    return rgbToHex3f(rgb.r, rgb.g, rgb.b, out);
}

int rgbToHex3f(float r, float g, float b, int* out)
{
    if(out == NULL)
        return -1;

    *out = 0x00 << 24 | (int)roundf(r * 255.f) << 16 | (int)roundf(g * 255.f) << 8 | (int)roundf(b * 255.f);

    return 0;
}

int rgbToHsl(rgb_t rgb, hsl_t* out)
{
    return rgbToHsl3f(rgb.r, rgb.g, rgb.b, out);
}

int rgbToHsl3f(float r, float g, float b, hsl_t* out)
{
    if(out == NULL)
        return -1;

    float max = r;
    if(g > max) max = g;
    if(b > max) max = b;

    float min = r;
    if(g < min) min = g;
    if(b < min) min = b;

    float d = max - min;
    float l = (max + min) / 2.f;

    float s = 0.f;
    if(d == 0.f) s = 0.f;
    else s = d / (1.f - fabs(2.f * l - 1.f));

    float h = 0.f;
    if(d != 0.f)
    {
        if(max == r) h = (g - b) / d;
        else if(max == g) h = (b - r) / d + 2;
        else h = (r - g) / d + 4;
    }

    h *= 0.16666666666667f;
    if(h < 0.f) h += 1.f;

    out->h = h;
    out->s = s;
    out->l = l;

    return 0;
}

int rgbToHsv(rgb_t rgb, hsv_t* out)
{
    return rgbToHsv3f(rgb.r, rgb.g, rgb.b, out);
}

int rgbToHsv3f(float r, float g, float b, hsv_t* out)
{
    if(out == NULL)
        return -1;

    float max = r;
    if(g > max) max = g;
    if(b > max) max = b;

    float min = r;
    if(g < min) min = g;
    if(b < min) min = b;

    float d = max - min;

    float h = 0.f;
    if(d != 0.f)
    {
        if(max == r) h = (g - b) / d;
        else if(max == g) h = (b - r) / d + 2;
        else h = (r - g) / d + 4;
    }

    h *= 0.16666666666667f;
    if(h < 0.f) h += 1.f;

    float s = 0.f;
    if(max == 0.f) s = 0.f;
    else s = d / max;

    out->h = h;
    out->s = s;
    out->v = max;

    return 0;
}

int rgbToHslsv(rgb_t rgb, hslsv_t* out)
{
    return rgbToHslsv3f(rgb.r, rgb.g, rgb.b, out);
}

int rgbToHslsv3f(float r, float g, float b, hslsv_t* out)
{
    if(out == NULL)
        return -1;

    float max = r;
    if(g > max) max = g;
    if(b > max) max = b;

    float min = r;
    if(g < min) min = g;
    if(b < min) min = b;

    float d = max - min;
    float l = (max + min) / 2.f;

    float s = 0.f;
    if(d == 0.f) s = 0.f;
    else s = d / (1.f - fabs(2.f * l - 1.f));

    float h = 0.f;
    if(d != 0.f)
    {
        if(max == r) h = (g - b) / d;
        else if(max == g) h = (b - r) / d + 2;
        else h = (r - g) / d + 4;
    }

    h *= 0.16666666666667f;
    if(h < 0.f) h += 1.f;

    float sv = 0.f;
    if(max == 0.f) sv = 0.f;
    else sv = d / max;

    out->h = h;
    out->s = s;
    out->l = l;
    out->sv = sv;
    out->v = max;

    return 0;
}

int rgbToHwb(rgb_t rgb, hwb_t* out)
{
    return rgbToHwb3f(rgb.r, rgb.g, rgb.b, out);
}

int rgbToHwb3f(float r, float g, float b, hwb_t* out)
{
    if(out == NULL)
        return -1;

    float max = r;
    if(g > max) max = g;
    if(b > max) max = b;

    float min = r;
    if(g < min) min = g;
    if(b < min) min = b;

    float d = max - min;

    float h = 0.f;
    if(d != 0.f)
    {
        if(max == r) h = (g - b) / d;
        else if(max == g) h = (b - r) / d + 2;
        else h = (r - g) / d + 4;
    }

    out->h = h;
    out->w = min;
    out->b = 1.f - max;

    return 0;
}

int rgbToCmyk(rgb_t rgb, cmyk_t* out)
{
    return rgbToCmyk3f(rgb.r, rgb.g, rgb.b, out);
}

int rgbToCmyk3f(float r, float g, float b, cmyk_t* out)
{
    if(out == NULL)
        return -1;

    float max = r;
    if(g > max) max = g;
    if(b > max) max = b;

    float c = 0.f;
    float m = 0.f;
    float y = 0.f;
    float k = 1.f - max;

    if(k < 1.f)
    {
        c = (1.f - r - k) / (1.f - k);
        m = (1.f - g - k) / (1.f - k);
        y = (1.f - b - k) / (1.f - k);
    }

    out->c = c;
    out->m = m;
    out->y = y;
    out->k = k;

    return 0;
}

int hexToRgb(int hex, rgb_t* out)
{
    if(out == NULL)
        return -1;

    byte r = (byte)(hex >> 16);
    byte g = (byte)(hex >> 8);
    byte b = (byte) hex;

    return hexToRgb3by(r, g, b, out);
}

int hexToRgb3by(byte r, byte g, byte b, rgb_t* out)
{
    if(out == NULL)
        return -1;

    out->r = (float)r / 255.f;
    out->g = (float)g / 255.f;
    out->b = (float)b / 255.f;

    return 0;
}

int hslToRgb(hsl_t hsl, rgb_t* out)
{
    return hslToRgb3f(hsl.h, hsl.s, hsl.l, out);
}

int hslToRgb3f(float h, float s, float l, rgb_t* out)
{
    if(out == NULL)
        return -1;

    static float _1_3rd = 1.f / 3.f;
    static float _2_3rd = 2.f / 3.f;
    static float _1_6th = 1.f / 6.f;

    if(s == 0.f)
    {
        out->r = l;
        out->g = l;
        out->b = l;
    }
    else
    {
        float tmp1 = 0.f;
        if(l < 0.5f) tmp1 = l * (1 + s);
        else tmp1 = l + s - l * s;

        float tmp2 = 2.f * l - tmp1;

        float tmpr = fmodx(h + _1_3rd, 1.f);
        float tmpg = h;
        float tmpb = fmodx(h - _1_3rd, 1.f);

        float r = 0.f;
        float g = 0.f;
        float b = 0.f;

        if(tmpr <= _1_6th) r = tmp2 + (tmp1 - tmp2) * 6.f * tmpr;
        else
        {
            if(tmpr <= 0.5f) r = tmp1;
            else
            {
                if(tmpr <= _2_3rd) r = tmp2 + (tmp1 - tmp2) * (_2_3rd - tmpr) * 6.f;
                else
                {
                    r = tmp2;
                }
            }
        }

        if(tmpg <= _1_6th) g = tmp2 + (tmp1 - tmp2) * 6.f * tmpg;
        else
        {
            if(tmpg <= 0.5f) g = tmp1;
            else
            {
                if(tmpg <= _2_3rd) g = tmp2 + (tmp1 - tmp2) * (_2_3rd - tmpg) * 6.f;
                else
                {
                    g = tmp2;
                }
            }
        }

        if(tmpb <= _1_6th) b = tmp2 + (tmp1 - tmp2) * 6.f * tmpb;
        else
        {
            if(tmpb <= 0.5f) b = tmp1;
            else
            {
                if(tmpb <= _2_3rd) b = tmp2 + (tmp1 - tmp2) * (_2_3rd - tmpb) * 6.f;
                else
                {
                    b = tmp2;
                }
            }
        }

        out->r = r;
        out->g = g;
        out->b = b;
    }

    return 0;
}

int hsvToRgb(hsv_t hsv, rgb_t* out)
{
    return hsvToRgb3f(hsv.h, hsv.s, hsv.v, out);
}

int hsvToRgb3f(float h, float s, float v, rgb_t* out)
{
    if(out == NULL)
        return -1;

    static float _1_3rd = 1.f / 3.f;
    static float _2_3rd = 2.f / 3.f;
    static float _1_6th = 1.f / 6.f;
    static float _5_6th = 5.f / 6.f;

    float c = v * s;
    float x = c * (1.f - fabs(fmodx(h * 6.f, 2.f) - 1.f));
    float m = v - c;

    float r, g, b;

    if(h < _1_6th) {
        r = c + m;
        g = x + m;
        b = m;
    }
    else if(h < _1_3rd) {
        r = x + m;
        g = c + m;
        b = m;
    }
    else if(h < 0.5f) {
        r = m;
        g = c + m;
        b = x + m;
    }
    else if(h < _2_3rd) {
        r = m;
        g = x + m;
        b = c + m;
    }
    else if(h < _5_6th) {
        r = x + m;
        g = m;
        b = c + m;
    }
    else {
        r = c + m;
        g = m;
        b = x + m;
    }

    out->r = r;
    out->g = g;
    out->b = b;

    return 0;
}

int hwbToRgb(hwb_t hwb, rgb_t* out)
{
    return hwbToRgb3f(hwb.h, hwb.w, hwb.b, out);
}

int hwbToRgb3f(float h, float w, float b, rgb_t* out)
{
    if(out == NULL)
        return -1;

    hslToRgb3f(h, 1.f, 0.5f, out);

    out->r = out->r * (1.f - w - b) + w;
    out->g = out->g * (1.f - w - b) + w;
    out->b = out->b * (1.f - w - b) + w;

    return 0;
}

int cmykToRgb(cmyk_t cmyk, rgb_t* out)
{
    return cmykToRgb4f(cmyk.c, cmyk.m, cmyk.y, cmyk.k, out);
}

int cmykToRgb4f(float c, float m, float y, float k, rgb_t* out)
{
    if(out == NULL)
        return -1;

    out->r = (1.f - c) * (1.f - k);
    out->g = (1.f - m) * (1.f - k);
    out->b = (1.f - y) * (1.f - k);

    return 0;
}