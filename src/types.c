/*
* types.c
* Type definitions.
*
* Author:   Sebastian Zander
* Created:  2019-03-20
*/

#include "types.h"

component_t component_default = { 
    .value = 0.f, 
    .component_type = CT_NONE, 
    .number_format = NF_FLOAT, 
    .leading_zeroes = 0, 
    .integer_digits = -1, 
    .decimal_digits = -1
};

colorformat_t colorformat_default = {
    .num_components = 0,
    .components = NULL
};

inputcolor_t inputcolor_default = { 
    .rgb = -1, 
    .color_string = NULL, 
    .num_components = 0,
    .components = NULL
};