/*
* parser.h
* Color and component value parsing functions.
*
* Author:   Sebastian Zander
* Created:  2019-03-20
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <math.h>

#include "utilities.h"
#include "converter.h"
#include "parser.h"

extern int verbosity_mode;

float parseComponentValue(const char* str)
{
    if(str == NULL || strlen(str) == 0)
        return -1.f;

    float value = -1;
    regex_t regex;
    int reti;
    char msgbuf[100];
    char databuf[16];
    int format = -1;
    float number = -1.f;

    const char* re = "^(([0-9]+)?(\\.[0-9]*)?)(|%|°)$|^0x([a-fA-F0-9]{1,2})$";

    /* compile regular expression */
    reti = regcomp(&regex, re, REG_EXTENDED | REG_ICASE);
    if(reti != 0) {
        fprintf(stderr, "could not compile regex\n");
        return -2.f;
    }

    const size_t max_groups = 6; /* including the whole matched pattern as group (index) 0 */
    regmatch_t regmatch[max_groups];

    /* execute regular expression */
    reti = regexec(&regex, str, max_groups, regmatch, 0);
    if(reti == 0) {
        //puts("match");
        for(int i = 1; i < max_groups; i++)
        {
            regmatch_t match = regmatch[i];
            if(match.rm_so == (size_t) - 1 || i == 2 || i == 3)
                continue;

            int len = match.rm_eo - match.rm_so;
            if(len > 15) len = 15;

            snprintf(databuf, len + 1, "%s", &str[match.rm_so]);
            //printf("   group %d: [%d-%d] %s\n", i, match.rm_so, match.rm_eo, databuf);

            // determine number format
            if(i == 4) {
                if(len == 0) format = 0;
                else if(len > 0 && strncmp(databuf, "%", 1) == 0) format = 1;
                else if(len > 0 && strncmp(databuf, "°", 1) == 0) format = 2;
            }
            // read multifunctional number group
            else if(i == 1) {
                number = atof(databuf);
            }
            // read hexdecimal number group
            else if(i == 5) {
                int hex = strtol(databuf, NULL, 16);
                if(hex > 0xff) hex = 0xff;
                number = (float)hex / 255.f;
                format = 3;
            }
        }

        // if(verbosity_mode == 1)
        //     printf("number format: %d\n", format);

        // convert the value respectively
        switch(format)
        {
            // decimal normal number and frations (percentages)
            case 0:
                // if number is a fraction take as is
                if(number <= 1.f) value = number;
                // if number is normal
                else value = number / 255.f;
                break;

            // decimal percentage (0%..100%)
            case 1:
                // convert percentage to fraction
                value = number / 100.f;
                break;

            // decimal degrees (0°..360°)
            case 2:
                // convert degrees to fraction
                value = number / 360.f;
                break;

            // hexadecimal number
            case 3:
                value = number;
                break;
        }

        value = clamp(value, 0.f, 1.f);
        
        // if(verbosity_mode == 1)
        //     printf("parsed number: %.4f\n", value);
    }
    else if(reti == REG_NOMATCH) {
        if(verbosity_mode == 1)
            printf("no match for component string %s\n", str);
    }
    else {
        regerror(reti, &regex, msgbuf, sizeof(msgbuf));
        fprintf(stderr, "match failed: %s\n", msgbuf);
        return -3.f;
    }

    /* free memory allocated to the pattern buffer by regcomp() */
    regfree(&regex);

    return value;
}

int parseColorString(const char* str, hsl_t* hsl_out, inputcolor_t* out)
{
    if(str == NULL || strlen(str) == 0 || out == NULL)
        return -1;

    rgb_t rgb = { -1.f, -1.f, -1.f };
    hsl_t hsl = { -1.f, -1.f, -1.f };
    regex_t regex;
    int reti;
    char msgbuf[100];
    char databuf[16];
    int format = -1;
    float c1 = 0;
    float c2 = 0;
    float c3 = 0;
    float c4 = 0;

    const char* re = "(#?)([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9\\.]{2})|(rgb|hsl|hsv|hwb)\\([ ]*([xa-fA-F0-9\\.\\°\\%]+),[ ]*([xa-fA-F0-9\\.\\°\\%]+),[ ]*([xa-fA-F0-9\\.\\°\\%]+)[ ]*\\)|(rgba|hsla|cmyk)\\([ ]*([xa-fA-F0-9\\.\\°\\%]+),[ ]*([xa-fA-F0-9\\.\\°\\%]+),[ ]*([xa-fA-F0-9\\.\\°\\%]+),[ ]*([xa-fA-F0-9\\.\\°\\%]+)[ ]*\\)";
    
    // if(verbosity_mode == 1)
    //     printf("parsing color string %s -- ", str);

    /* compile regular expression */
    reti = regcomp(&regex, re, REG_EXTENDED | REG_ICASE);
    if(reti != 0) {
        fprintf(stderr, "could not compile regex\n");
        return -2;
    }

    const size_t max_groups = 13; /* including the whole matched pattern as group (index) 0 */
    regmatch_t regmatch[max_groups];

    /* execute regular expression */
    reti = regexec(&regex, str, max_groups, regmatch, 0);
    if(reti == 0) {
        // if(verbosity_mode == 1)
        //     puts("match");

        for(int i = 1; i < max_groups; i++)
        {
            regmatch_t match = regmatch[i];
            if(match.rm_so == (size_t) - 1)
                continue;

            int len = match.rm_eo - match.rm_so;
            if(len > 15) len = 15;

            // strncpy(databuf, &str[match.rm_so], len);
            snprintf(databuf, len + 1, "%s", &str[match.rm_so]);

            // if(verbosity_mode == 1)
            //     printf("   group %d: [%d-%d] %s\n", i, match.rm_so, match.rm_eo, databuf);

            if(i == 5)
            {
                if(strcmp(databuf, "rgb") == 0) format = 1;
                else if(strcmp(databuf, "hsl") == 0) format = 2;
                else if(strcmp(databuf, "hsv") == 0) format = 3;
                else if(strcmp(databuf, "hwb") == 0) format = 4;
            }

            if(i == 9)
            {
                if(strcmp(databuf, "rgba") == 0) format = 1;
                else if(strcmp(databuf, "hsla") == 0) format = 2;
                else if(strcmp(databuf, "cmyk") == 0) format = 5;
            }

            switch(i)
            {
                case 2:
                    c1 = (float)strtol(databuf, NULL, 16) / 255.f;
                    format = 0;
                    break;

                case 3:
                    c2 = (float)strtol(databuf, NULL, 16) / 255.f;
                    format = 0;
                    break;

                case 4:
                    c3 = (float)strtol(databuf, NULL, 16) / 255.f;
                    format = 0;
                    break;

                /* R or H */
                case 6:
                    c1 = parseComponentValue(databuf);
                    break;

                /* G, S or BL */
                case 7:
                    c2 = parseComponentValue(databuf);
                    break;

                /* B, V, L or W */
                case 8:
                    c3 = parseComponentValue(databuf);
                    break;

                /* R, H or C */
                case 10:
                    c1 = parseComponentValue(databuf);
                    break;

                /* G, S or M */
                case 11:
                    c2 = parseComponentValue(databuf);
                    break;

                /* B, L or Y */
                case 12:
                    c3 = parseComponentValue(databuf);
                    break;

                /* A or K */
                case 13:
                    c4 = parseComponentValue(databuf);
                    break;
            }
        }

        // convert components to red, green and blue
        switch(format)
        {
            // rgb components
            case 0:
            case 1:
                rgb.r = c1;
                rgb.g = c2;
                rgb.b = c3;
                rgbToHsl(rgb, &hsl);
                break;

            // hsl components
            case 2:
                hsl.h = c1;
                hsl.s = c2;
                hsl.l = c3;
                hslToRgb3f(c1, c2, c3, &rgb);
                break;

            // hsv components
            case 3:
                hsvToRgb3f(c1, c2, c3, &rgb);
                rgbToHsl(rgb, &hsl);
                break;

            // hwb components
            case 4:
                hwbToRgb3f(c1, c2, c3, &rgb);
                rgbToHsl(rgb, &hsl);
                break;

            // cmyk components
            case 5:
                cmykToRgb4f(c1, c2, c3, c4, &rgb);
                rgbToHsl(rgb, &hsl);
                break;
        }

        // if(verbosity_mode == 1)
        //     printf("parsed rgb value: 0x%08x\n", rgb);
    }
    else if(reti == REG_NOMATCH) {
        if(verbosity_mode == 1)
            printf("no match for color string %s\n", str);
    }
    else {
        regerror(reti, &regex, msgbuf, sizeof(msgbuf));
        fprintf(stderr, "match failed: %s\n", msgbuf);
        return -3;
    }

    /* free memory allocated to the pattern buffer by regcomp() */
    regfree(&regex);

    out->rgb = rgb;
    out->hsl = hsl;

    if(hsl_out != NULL) {
        hsl_out->h = hsl.h;
        hsl_out->s = hsl.s;
        hsl_out->l = hsl.l;
    }

    return 0;
}

int formatOutputColor(rgb_t rgb, const char* output_format_string, char* output_buffer, int buffer_length)
{
    if(output_format_string == NULL || strlen(output_format_string) == 0)
        return -1;

    regex_t regex;
    int reti;
    char msgbuf[100];
    char databuf[16];
    const int fmtbuf_length = 32;
    char fmtbuf[fmtbuf_length];
    int output_buffer_overflow = 0;

    const char* re = "%(R|G|BL|B|H|SV|S|L|V|W|C|M|Y|K)(0?)([1-9][0-9]*)?(\\.([0-9]+))?(x|f|deg|d|p|%%|°)";
    
    /* compile regular expression */
    reti = regcomp(&regex, re, REG_EXTENDED | REG_ICASE);
    if(reti != 0) {
        fprintf(stderr, "could not compile regex\n");
        return -2;
    }

    const size_t max_groups = 7; /* including the whole matched pattern as group (index) 0 */
    const size_t max_matches = 16; /* maximum number of component placeholders to replace in output format */
    size_t num_matches = 0;
    regmatch_t regmatch[max_groups];
    char* format_cursor = (char*)output_format_string;
    regoff_t output_cursor = 0;
    regoff_t last_eo = 0;
    regoff_t global_offset = 0;

    hslsv_t hslsv = { 0.f, 0.f, 0.f, 0.f, 0.f };
    int hslsv_calculated = 0;
    hwb_t hwb = { 0.f, 0.f, 0.f };
    int hwb_calculated = 0;
    cmyk_t cmyk = { 0.f, 0.f, 0.f, 0.f };
    int cmyk_calculated = 0;

    // run through the maximum number of matches
    for(int m = 0; m < max_matches; m++)
    {
        /* execute regular expression */
        reti = regexec(&regex, format_cursor, max_groups, regmatch, 0);
        if(reti == 0) {
            num_matches++;

            global_offset = (regoff_t)(format_cursor - output_format_string);
            // copy normal text between the matches to the output buffer
            regoff_t text_between_length = global_offset + regmatch[0].rm_so - last_eo;
            if(text_between_length > 0)
            {
                int available_length = buffer_length - output_cursor - 1;
                if(available_length < text_between_length)
                {
                    snprintf(output_buffer + output_cursor, available_length + 1, "%s", format_cursor);
                    output_cursor += available_length;
                    output_buffer_overflow = 1;
                    break;
                }
                else
                {
                    snprintf(output_buffer + output_cursor, text_between_length + 1, "%s", format_cursor);
                    output_cursor += text_between_length;
                }
            }

            float value = -1.f;
            int leading_zeroes = -1;
            int integer_digits = -1;
            int decimal_digits = -1;
            char number_format = 'd';

            // if(verbosity_mode == 1)
            // {
            //     int len = regmatch[0].rm_eo - regmatch[0].rm_so;
            //     if(len > 15) len = 15;
            //     snprintf(databuf, len + 1, "%s", &format_cursor[regmatch[0].rm_so]);
            //     printf("parsed component string: \033[91m%s\033[00m\n", databuf);
            // }
            
            // run through all groups in the current match
            for(int i = 1; i < max_groups; i++)
            {
                regmatch_t match = regmatch[i];
                if(match.rm_so == (size_t) - 1)
                    continue;

                int len = match.rm_eo - match.rm_so;
                if(len > 0)
                {
                    if(len > 15) len = 15;

                    // strncpy(databuf, &str[match.rm_so], len);
                    snprintf(databuf, len + 1, "%s", &format_cursor[match.rm_so]);

                    // if(verbosity_mode == 1)
                    //     printf("   match \033[92m%d\033[00m group \033[93m%d\033[00m: [%d-%d] \033[91m%s\033[00m\n", m, i, global_offset + match.rm_so, 
                    //         global_offset + match.rm_eo, databuf);

                    // color component
                    if(i == 1) {
                        if(strcmp(databuf, "R") == 0) value = rgb.r;
                        else if(strcmp(databuf, "G") == 0) value = rgb.g;
                        else if(strcmp(databuf, "B") == 0) value = rgb.b;
                        else if(strcmp(databuf, "H") == 0)
                        {
                            if(hslsv_calculated == 0) {
                                rgbToHslsv(rgb, &hslsv);
                                hslsv_calculated = 1;
                            }
                            value = hslsv.h;
                        }
                        else if(strcmp(databuf, "S") == 0)
                        {
                            if(hslsv_calculated == 0) {
                                rgbToHslsv(rgb, &hslsv);
                                hslsv_calculated = 1;
                            }
                            value = hslsv.s;
                        }
                        else if(strcmp(databuf, "L") == 0)
                        {
                            if(hslsv_calculated == 0) {
                                rgbToHslsv(rgb, &hslsv);
                                hslsv_calculated = 1;
                            }
                            value = hslsv.l;
                        }
                        else if(strcmp(databuf, "SV") == 0)
                        {
                            if(hslsv_calculated == 0) {
                                rgbToHslsv(rgb, &hslsv);
                                hslsv_calculated = 1;
                            }
                            value = hslsv.sv;
                        }
                        else if(strcmp(databuf, "V") == 0)
                        {
                            if(hslsv_calculated == 0) {
                                rgbToHslsv(rgb, &hslsv);
                                hslsv_calculated = 1;
                            }
                            value = hslsv.v;
                        }
                        else if(strcmp(databuf, "W") == 0)
                        {
                            if(hwb_calculated == 0) {
                                rgbToHwb(rgb, &hwb);
                                hwb_calculated = 1;
                            }
                            value = hwb.w;
                        }
                        else if(strcmp(databuf, "BL") == 0)
                        {
                            if(hwb_calculated == 0) {
                                rgbToHwb(rgb, &hwb);
                                hwb_calculated = 1;
                            }
                            value = hwb.b;
                        }
                        else if(strcmp(databuf, "C") == 0)
                        {
                            if(cmyk_calculated == 0) {
                                rgbToCmyk(rgb, &cmyk);
                                cmyk_calculated = 1;
                            }
                            value = cmyk.c;
                        }
                        else if(strcmp(databuf, "M") == 0)
                        {
                            if(cmyk_calculated == 0) {
                                rgbToCmyk(rgb, &cmyk);
                                cmyk_calculated = 1;
                            }
                            value = cmyk.m;
                        }
                        else if(strcmp(databuf, "Y") == 0)
                        {
                            if(cmyk_calculated == 0) {
                                rgbToCmyk(rgb, &cmyk);
                                cmyk_calculated = 1;
                            }
                            value = cmyk.y;
                        }
                        else if(strcmp(databuf, "K") == 0)
                        {
                            if(cmyk_calculated == 0) {
                                rgbToCmyk(rgb, &cmyk);
                                cmyk_calculated = 1;
                            }
                            value = cmyk.k;
                        }
                        else {
                            fprintf(stderr, "invalid component specifier: \033[91m%s\033[00m\n", databuf);
                            return -4;
                        }
                    }
                    // integer digits
                    else if(i == 2)
                    {
                        // if we get to this point it means that leading zeroes were specified
                        leading_zeroes = 1;
                    }
                    // integer digits
                    else if(i == 3)
                    {
                        integer_digits = atoi(databuf);
                    }
                    // decimal digits
                    else if(i == 5)
                    {
                        decimal_digits = atoi(databuf);
                    }
                    // number format
                    else if(i == 6)
                    {
                        if(strcmp(databuf, "d") == 0) number_format = 'd';
                        else if(strcmp(databuf, "f") == 0) number_format = 'f';
                        else if(strcmp(databuf, "x") == 0) number_format = 'x';
                        else if(strcmp(databuf, "p") == 0 || strcmp(databuf, "%%") == 0) number_format = 'p';
                        else if(strcmp(databuf, "\u00b0") == 0 || strcmp(databuf, "deg") == 0) number_format = 'g';
                        else {
                            fprintf(stderr, "invalid number format specifier: \033[91m%s\033[00m\n", databuf);
                            return -5;
                        }
                    }
                }
            }

            // if(verbosity_mode == 1)
            //     printf("parsed component attributes: \033[92m%.4f, %d, %d, %d, %c\033[00m\n", value, leading_zeroes, 
            //         integer_digits, decimal_digits, number_format);

            // build format string with the parsed component value attributes
            int fmtbuf_cursor = 0;
            //fmtbuf_cursor += snprintf(fmtbuf + fmtbuf_cursor, fmtbuf_length - fmtbuf_cursor, "%");
            strcpy(fmtbuf + fmtbuf_cursor, "%");
            fmtbuf_cursor++;
            if(leading_zeroes == 1)
                fmtbuf_cursor += snprintf(fmtbuf + fmtbuf_cursor, fmtbuf_length - fmtbuf_cursor, "0");
            if(integer_digits > -1)
                fmtbuf_cursor += snprintf(fmtbuf + fmtbuf_cursor, fmtbuf_length - fmtbuf_cursor, "%d", integer_digits);
            if(decimal_digits > -1 && (number_format == 'f' || number_format == 'p' || number_format == 'g'))
                fmtbuf_cursor += snprintf(fmtbuf + fmtbuf_cursor, fmtbuf_length - fmtbuf_cursor, ".%d", decimal_digits);
            if(number_format == 'd')
                fmtbuf_cursor += snprintf(fmtbuf + fmtbuf_cursor, fmtbuf_length - fmtbuf_cursor, "d");
            else if(number_format == 'f' || number_format == 'p' || number_format == 'g')
                fmtbuf_cursor += snprintf(fmtbuf + fmtbuf_cursor, fmtbuf_length - fmtbuf_cursor, "f");
            else if(number_format == 'x')
                fmtbuf_cursor += snprintf(fmtbuf + fmtbuf_cursor, fmtbuf_length - fmtbuf_cursor, "x");

            // format number value and copy into output buffer
            global_offset = (regoff_t)(format_cursor - output_format_string);
            int bytes_written = 0;
            int available_length = buffer_length - output_cursor - 1;
            if(number_format == 'd')
                bytes_written += snprintf(output_buffer + output_cursor, available_length + 1, fmtbuf, (int)roundf(value * 255.f));
            else if(number_format == 'f')
                bytes_written += snprintf(output_buffer + output_cursor, available_length + 1, fmtbuf, value);
            else if(number_format == 'p')
                bytes_written += snprintf(output_buffer + output_cursor, available_length + 1, fmtbuf, value * 100.f);
            else if(number_format == 'g')
                bytes_written += snprintf(output_buffer + output_cursor, available_length + 1, fmtbuf, value * 360.f);
            else if(number_format == 'x')
                bytes_written += snprintf(output_buffer + output_cursor, available_length + 1, fmtbuf, (int)roundf(value * 255.f));
            output_cursor += bytes_written;

            if(bytes_written > available_length) {
                output_buffer_overflow = 1;
                break;
            }

            // advance the cursor to the end index of the current group
            format_cursor += regmatch[0].rm_eo;
            last_eo = global_offset + regmatch[0].rm_eo;
        }
        else if(reti == REG_NOMATCH) {
            break;
        }
        else {
            regerror(reti, &regex, msgbuf, sizeof(msgbuf));
            fprintf(stderr, "match failed: %s\n", msgbuf);
            break;
        }
    }

    global_offset = (regoff_t)(format_cursor - output_format_string);
    // copy normal text after the last match to the output buffer
    regoff_t text_after_length = strlen(output_format_string) - last_eo;
    if(text_after_length > 0)
    {
        int available_length = buffer_length - output_cursor - 1;
        int bytes_written = 0;
        bytes_written = snprintf(output_buffer + output_cursor, available_length + 1, "%s", format_cursor);

        if(bytes_written > available_length) {
            output_buffer_overflow = 1;
        }
    }

    // if(verbosity_mode == 1)
    //     printf("hslsv = (%0.4f, %0.4f, %0.4f, %0.4f, %0.4f)\n", hslsv[0], hslsv[1], hslsv[2], hslsv[3], hslsv[4]);

    // if(verbosity_mode == 1) {
    //     printf("remaining output buffer length: %d\n", buffer_length - output_cursor);
    //     printf("output buffer string length: %ld\n", strlen(output_buffer));
    // }

    /* free memory allocated to the pattern buffer by regcomp() */
    regfree(&regex);

    // if the output format does not contain any placeholder
    if(num_matches == 0)
    {
        snprintf(output_buffer, buffer_length, "%s", output_format_string);
    }

    return output_buffer_overflow == 1 ? 2 : 1;
}