/*
* colerp.c
* A program that interpolates color values.
*
* Author:   Sebastian Zander
* Created:  2019-03-16
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <curses.h>

#include "defines.h"
#include "types.h"
#include "utilities.h"
#include "converter.h"
#include "parser.h"

int verbosity_mode = 0;

extern evalfun eval;
extern inputcolor_t inputcolor_default;

int printLineBreakingText(FILE* stream, const char* text, int left_margin, int right_margin, int indent);
int printUsageHints();
int printExamples();
int printVersionInformation();

int printLineBreakingText(FILE* stream, const char* text, int left_margin, int right_margin, int indent)
{
    struct winsize terminal_size;

    // receive terminal size to format output with a dynamic width
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &terminal_size);
    const int terminal_width = terminal_size.ws_col < MAX_TEXT_WIDTH ? terminal_size.ws_col : MAX_TEXT_WIDTH;
    int available_width = terminal_width - left_margin - right_margin;

    int text_len = 0;
    if(text != NULL && (text_len = strlen(text)) > 0)
    {
        if(text_len <= available_width)
        {
            if(left_margin > 0)
                fprintf(stream, "%-*s%s\n", left_margin, " ", text);
            else
                fprintf(stream, "%s\n", text);
        }
        else
        {
            char* cursor = (char*)text;
            char* it = cursor;
            char* last_it = it;
            int distance = 0;
            int distance_from_start = 0;
            int line = 0;
            int section_len = 0;

            // iterate through the text until the end
            while(distance_from_start <= text_len)
            {
                // remember the cursor position of the last whitespace
                last_it = it;
                section_len = -1;

                // run through the text until the next whitespace or until the end
                while(*it != ' ' && *it != '\t' && *it != '\n' && *it != '\0') it++;
                distance = it - cursor;

                // determine partial text section length until the last whitespace before the line overflow
                if(distance > available_width)
                    section_len = last_it - cursor;
                // ..or until the next line break or the end of the text
                else if(*it == '\0' || *it == '\n')
                    section_len = it - cursor;

                // print the partial text section
                if(section_len >= 0)
                {
                    if(left_margin > 0)
                        fprintf(stream, "%-*s%-.*s\n", left_margin, " ", section_len, cursor);
                    else
                        fprintf(stream, "%-.*s\n", section_len, cursor);
                    line++;
                }
                // else if(section_len == 0)
                // {
                //     fprintf(stream, "\n");
                //     line++;
                // }

                if(distance > available_width)
                    cursor = last_it;
                else if(*it == '\n') {
                    it++;
                    cursor = it;
                }
                else if(*it == '\0')
                    break;
                else
                    it++;

                distance_from_start = cursor - text;
            }
        }
    }

    return 0;
}

int printUsageHints()
{
    struct winsize terminal_size;

    // receive terminal size to format output with a dynamic width
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &terminal_size);
    const int terminal_width = terminal_size.ws_col < MAX_TEXT_WIDTH ? terminal_size.ws_col : MAX_TEXT_WIDTH;

    optionentry_t option_entries[] = {
    //  { "-i", "--input-color", "Input color string to be processed (overrides standard input)" }, 
        { "-o", "--output-format", "C-inspired formatting string for number format and decimal notation of the output color. "
            "Recreates the input color format if unset." }, 
        { "-H", "--hue", "Target hue value (either absolute or relative)" }, 
        { "-S", "--saturation", "Target saturation value (either absolute or relative)" }, 
        { "-L", "--lightness", "Target lightness value (either absolute or relative)" }, 
        { NULL, "--match-hue", "The hue given as a span around a mean to be matched for interpolation and mutation" }, 
        { NULL, "--match-saturation", "The saturation given as a span around a mean to be matched for interpolation and mutation" }, 
        { NULL, "--match-lightness", "The lightness given as a span around a mean to be matched for interpolation and mutation" }, 
        { "-e", "--evaluator", "Evaluator function used for color interpolation and mixing" }, 
        { "-p", "--power", "Global multiplicator used on the determined interpolation factor" }, 
        { "-v", "--verbosity-mode", "Defines the detail level of the generated output" }, 
        { "-h", "--help", "Prints help (this message) and exits" }, 
        { NULL, "--examples", "Prints common usage examples and exits" }, 
        { NULL, "--version", "Prints program version and exits" }, 
    };

    int num_option_entries = (int)(sizeof(option_entries) / sizeof(char*) / 3);

    printf(
        "\033[1mUsage:\033[0m colerp [option]... [color input string]...\n"
        "Interpolates and mutates color values and converts between different color formats.\n\n"
    );

    // define standard table and column dimensions
    static int columns_measured = 0;
    const int left_margin = 2;
    const int column_padding = 2;
    const int right_margin = 2;
    static int col1_width = 2;
    static int col2_width = 21;
    const int col3_min_width = 30;

    if(columns_measured == 0)
    {
        // iterate over all options to find maximum string lengths of short and long forms
        for(int i = 0; i < num_option_entries; i++)
        {
            optionentry_t entry = option_entries[i];
            int short_len = entry.short_form != NULL ? strlen(entry.short_form) : 0;
            int long_len = entry.long_form != NULL ? strlen(entry.long_form) : 0;
            if(short_len > col1_width) col1_width = short_len;
            if(long_len > col2_width) col2_width = long_len;
        }

        columns_measured = 1;
    }

    int col3_offset = left_margin + col1_width + column_padding + col2_width;
    int col3_available_space = terminal_width - col3_offset - right_margin - column_padding;

    // run through all options
    for(int i = 0; i < num_option_entries; i++)
    {
        optionentry_t entry = option_entries[i];
        int outputs = 0;

        // short form
        if(entry.short_form != NULL && strlen(entry.short_form) > 0) {
            printf("%-*s%-*s", left_margin, " ", col1_width, entry.short_form);
            outputs++;
        }
        else
            printf("%*s", left_margin + col1_width, " ");

        // long form
        if(entry.long_form != NULL && strlen(entry.long_form) > 0) {
            printf("%-*s\033[96m%-*s\033[0m", column_padding, outputs > 0 ? "," : " ", col2_width, entry.long_form);
            outputs++;
        }
        else
            printf("%*s", column_padding + col2_width, " ");

        // description
        int description_len = 0;
        if(entry.description != NULL && (description_len = strlen(entry.description)) > 0)
        {
            if(description_len <= col3_available_space)
                printf("%-*s%s\n", column_padding, " ", entry.description);
            else
            {
                char* cursor = (char*)entry.description;
                char* it = cursor;
                char* last_it = it;
                int distance = 0;
                int distance_from_start = 0;
                int line = 0;

                // iterate through the description until the end
                while(distance_from_start <= description_len)
                {
                    // remember the cursor position of the last whitespace
                    last_it = it;

                    // run through the description until the next whitespace or until the end
                    while(*it != ' ' && *it != '\t' && *it != '\n' && *it != '\0') it++;
                    distance = it - cursor;

                    //fprintf(stderr, "whitespace at %ld (%ld)\n", it - entry.description, it - cursor);
                    //fflush(stderr);

                    // print the partial text section and advance the cursor
                    if(distance > col3_available_space || *it == '\0')
                    {
                        int section_len = 0;

                        // determine partial text section length and print that much text
                        if(distance <= col3_available_space && *it == '\0')
                            section_len = it - cursor;
                        else
                            section_len = last_it - cursor;

                        // if this is a line after the initial line fill previous columns with whitespace
                        if(line > 0)
                            printf("%-*s", col3_offset, " ");
                        printf("%-*s%-.*s\n", column_padding, " ", section_len, cursor);

                        line++;

                        // advance the cursor unless we have reached the end
                        if(distance <= col3_available_space && *it == '\0')
                            break;
                        else
                            cursor = last_it;
                    }
                    else
                        it++;

                    distance_from_start = cursor - entry.description;
                }
            }
            outputs++;
        }
    }

    printf("\n");

    return 0;
}

int printExamples()
{
    struct winsize terminal_size;

    // receive terminal size to format output with a dynamic width
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &terminal_size);
    const int terminal_width = terminal_size.ws_col < MAX_TEXT_WIDTH ? terminal_size.ws_col : MAX_TEXT_WIDTH;

    exampleentry_t example_entries[] = {
        { "colerp \"#38a2ff\" \"#6cd12d\" -o\"hsl(%H.1deg°, %S.1p%, %L.1p%)\"", 
            "Converts the two given hex-color values into the specified HSL-output-format and prints it to standard output." }, 
        { "colerp \"#38a2ff\" -S-30% -L+50%", 
            "Lowers the saturation by 30 percent and increases the lightness of the given input hex-color by 50 percent." }, 
        { "colerp \"#38a2ff\" -S-30% -L+50% --match-hue=200°-220°", 
            "Like the previous example but only changes the input color if its hue is in the range from 200 to 220 degrees. "
            "All \"--match..\" options determine the color mixing factor using an evaluator function that can be changed by "
            "specifying the \"-e, --evaluator\" option (see next example)." }, 
        { "colerp \"#38a2ff\" -S-30% -L+50% --match-hue=210°,20° -esin", 
            "Like the previous example but using a sine-wave based evaluator function for mixing factor determination. "
            "Simply execute \033[096mcolerp -e\033[0m to see what functions are offered for selection." }, 
        { "colerp -h--match-hue", 
            "Gives help and further usage examples on the \"--match-hue\" option." }, 
    };

    int num_example_entries = (int)(sizeof(example_entries) / sizeof(char*) / 2);

    printf(
        "\033[1mExamples:\033[0m\n"
    );

    // define standard table and column dimensions
    const int left_margin = 2;
    const int right_margin = 2;
    int full_available_space = terminal_width - left_margin - right_margin;

    // run through all examples
    for(int i = 0; i < num_example_entries; i++)
    {
        exampleentry_t entry = example_entries[i];

        printf("%-*s\033[096m%s\033[0m\n", left_margin, " ", entry.command);

        // description
        int description_len = 0;
        if(entry.description != NULL && (description_len = strlen(entry.description)) > 0)
        {
            if(description_len <= full_available_space)
                printf("%-*s%s\n", left_margin, " ", entry.description);
            else
            {
                char* cursor = (char*)entry.description;
                char* it = cursor;
                char* last_it = it;
                int distance = 0;
                int distance_from_start = 0;

                // iterate through the description until the end
                while(distance_from_start <= description_len)
                {
                    // remember the cursor position of the last whitespace
                    last_it = it;

                    // run through the description until the next whitespace or until the end
                    while(*it != ' ' && *it != '\t' && *it != '\0') it++;
                    distance = it - cursor;

                    // print the partial text section and advance the cursor
                    if(distance > full_available_space || *it == '\0')
                    {
                        int section_len = 0;

                        // determine partial text section length and print that much text
                        if(distance <= full_available_space && *it == '\0')
                            section_len = it - cursor;
                        else
                            section_len = last_it - cursor;

                        // if this is a line after the initial line fill previous columns with whitespace
                        printf("%-*s%-.*s\n", left_margin, " ", section_len, cursor);

                        // advance the cursor unless we have reached the end
                        if(distance <= full_available_space && *it == '\0')
                            break;
                        else
                            cursor = last_it;
                    }
                    else
                        it++;

                    distance_from_start = cursor - entry.description;
                }
            }
            printf("\n");
        }
    }

    return 0;
}

int printVersionInformation()
{
    printf(
        "\033[1mColerp %d.%d.%d\033[0m Build %d (compiled %s)\n"
        "Originally written by Sebastian Zander (colerp@sebastianzander.de; @sepplroy)\n"
        "License: GNU General Public License version 3 or later (http://gnu.org/licenses/gpl.html)\n"
        "This is free software. You are free to change and redistribute it.\n"
        "There is no warrenty to the extent permitted by law.\n"
        "Public GitLab repository: https://gitlab.com/sebastianzander/colerp\n\n"
       , VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_BUILD, VERSION_COMPILED
    );

    return 0;
}

int main(int argc, char* argv[])
{
    //initscr();

    int i;
    int num_non_options = 0;

    float power_factor = 1.f;
    char source_color_string[256];
    rgb_t rgb_source = { 0.f, 0.f, 0.f };
    rgb_t rgb_result = { 0.f, 0.f, 0.f };
    hsl_t hsl_source = { 0.f, 0.f, 0.f };
    hsl_t hsl_target = { 0.f, 0.f, 0.f };
    hsl_t hsl_result = { 0.f, 0.f, 0.f };
    inputcolor_t* input_colors = NULL;
    int num_actual_input_colors = 0;

    float h_target = 0.f;
    float s_target = 0.f;
    float l_target = 0.f;

    float h_change = 0.f;
    float s_change = 0.f;
    float l_change = 0.f;

    float h_mean = -1.f;
    float h_span = -1.f;
    float s_mean = -1.f;
    float s_span = -1.f;
    float l_mean = -1.f;
    float l_span = -1.f;

    int h_target_set = 0;
    int s_target_set = 0;
    int l_target_set = 0;
    int source_color_set = 0;
    int source_color_format = 0;
    char* output_format_string = NULL;
    int output_format_set = 0;
    int mutated = 0;

    // process program arguments (first pass)
    for(i = 1; i < argc; i++)
    {
        char* arg = argv[i];
        int arg_len = strlen(argv[i]);

        // count all non-options
        if(strncmp(arg, "-", 1) != 0)
            num_non_options++;

        if(strncmp(arg, "--verbosity-mode", 16) == 0 || strncmp(arg, "-v", 2) == 0)
        {
            int n = 0;
            if(
                (strncmp(arg, "--verbosity-mode=", n = 17) == 0 && arg_len > n) || 
                (strncmp(arg, "-v=", n = 3) == 0 && arg_len > n) || 
                (strncmp(arg, "-v", n = 2) == 0 && arg_len > n && arg[n] != '='))
            {
                // extract value
                char* val = &arg[n];
                verbosity_mode = atoi(val);
                if(verbosity_mode < 0 || verbosity_mode > 2)
                {
                    fprintf(stderr, 
                        "Invalid value <%d>\n"
                        "  -v<mode>, --verbosity-mode=<mode> [0|1|2]\n"
                        , verbosity_mode
                    );
                    verbosity_mode = 0;
                }
            }
            else
            {
                fprintf(stderr, 
                    "Missing value:\n"
                    "  -v<mode>, --verbosity-mode=<mode> [0|1|2]\n"
                    "\n"
                );
            }
        }
        /* HELP */
        else if(strcmp(arg, "--help") == 0 || strcmp(arg, "-h") == 0)
        {
            printUsageHints();
            return 0;
        }
        /* EXAMPLES */
        else if(strcmp(arg, "--examples") == 0)
        {
            printExamples();
            return 0;
        }
        /* VERSION */
        else if(strcmp(arg, "--version") == 0)
        {
            printVersionInformation();
            return 0;
        }
    }

    // allocate memory for variable count of color inputs
    input_colors = (inputcolor_t*)malloc(num_non_options * sizeof(inputcolor_t));
    if(input_colors == NULL)
    {
        fprintf(stderr, 
            "Error: could not allocate %ld Bytes of memory for input values.\n", num_non_options * sizeof(int));
        return -2;
    }

    // process program arguments (second pass)
    for(i = 1; i < argc; i++)
    {
        char* arg = argv[i];
        int arg_len = strlen(argv[i]);

        /* INPUT COLOR */
        if(strncmp(arg, "--input-color", 13) == 0 || strncmp(arg, "-i", 2) == 0)
        {
            int n = 0;
            if(
                (strncmp(arg, "--input-color=", n = 14) == 0 && arg_len > n) || 
                (strncmp(arg, "-i=", n = 3) == 0 && arg_len > n) || 
                (strncmp(arg, "-i", n = 2) == 0 && arg_len > n && arg[n] != '='))
            {
                // extract value
                char* val = &arg[n];
                inputcolor_t input_color = inputcolor_default;
                int result = parseColorString(val, NULL, &input_color);

                if(result < 0) {
                    fprintf(stderr, 
                        "The given value \033[96m%s\033[00m could not be interpreted as a valid color value!\n", 
                        val);
                    return -2;
                }
                else if(verbosity_mode == 1)
                    printf("Specified input color: \033[96m%s\033[00m\n", val);

                source_color_set = 1;
            }
            // missing value
            else
            {
                fprintf(stderr, 
                    "Missing value:\n"
                    "  -i<color>, --input-color=<color>\n\n"
                    "<color>\n"
                    "  A color value in one of the following formats:\n"
                    "  #rrggbb | rgb(r,g,b) | hsl(h,s,l) | hsv(h,s,v) | cmyk(c,m,yk)\n"
                    "  For information about valid components see the next paragraph.\n\n"
                    "Component values of Hex, RGB, HSL, HSV and CYMK may be specified in one of the following number formats:\n"
                    "  Percentage:     in range [0..1] valid for components r,g,b,h,s,l,v\n"
                    "                  in range [0%%..100%%] valid for components r,g,b,h,s,l,v\n"
                    "  Decimal:        in range [0..255] valid for components r,g,b,h,s,l,v\n"
                    "  Hexadecimal:    in range [0x00..0xff] valid for components r,g,b,h,s,l,v\n"
                    "  Degrees:        in range [0°..359°] valid for component h\n\n"
                    "The following example represents the same color value in all possible formats:\n"
                    "  #38a2ff\n"
                    "  rgb(0x38,0xa2,0xff)   rgb(56,162,255)    rgb(0.219,0.635,1.0)   rgb(21.9%%,63.5%%,100%%)\n"
                    "  hsl(0x93,0xc7,0xff)   hsl(147,199,255)   hsl(0.578,0.785,1.0)   hsl(208°,78.5%%,100%%)\n"
                    "  hsv(0x93,0xff,0x9c)   hsv(147,255,156)   hsv(0.578,1.0,0.61)    hsv(208°,100%%,61%%)\n"
                    "  cmyk(78%%,36%%,0%%,0%%)\n"
                    "\n"
                );
                return -1;
            }
        }
        /* OUTPUT FORMAT */
        else if(strncmp(arg, "--output-format", 15) == 0 || strncmp(arg, "-o", 2) == 0)
        {
            int n = 0;
            if(
                (strncmp(arg, "--output-format=", n = 16) == 0 && arg_len > n) || 
                (strncmp(arg, "-o=", n = 3) == 0 && arg_len > n) || 
                (strncmp(arg, "-o", n = 2) == 0 && arg_len > n && arg[n] != '='))
            {
                output_format_string = &arg[n];
                if(strcmp(output_format_string, "none") == 0) output_format_set = 2;
                else output_format_set = 1;
            }
            // missing value
            else
            {
                fprintf(stderr, 
                    "Missing value:\n"
                    "  -o<format>, --output-format=<format>\n\n"
                    "<format>\n"
                    "  A formatting string containing one or multiple color component placeholders\n"
                    "  including specification of decimal notation and number format.\n"
                    "\n"
                );
                return -1;
            }
        }
        /* MATCH HUE */
        else if(strcmp(arg, "--match-hue") == 0)
        {
            if(i + 2 <= argc - 1 && argv[i+1][0] != '-' && argv[i+2][0] != '-')
            {
                i++;
                h_mean = atof(argv[i]);
                i++;
                h_span = atof(argv[i]);
            }
            else
            {
                fprintf(stderr, 
                    "Missing values:\n"
                    "  --match-hue=<mean value>,<span value>\n"
                    "\n"
                );
                return -1;
            }
        }
        /* MATCH SATURATION */
        else if(strcmp(arg, "--match-saturation") == 0)
        {
            if(i + 2 <= argc - 1 && argv[i+1][0] != '-' && argv[i+2][0] != '-')
            {
                i++;
                s_mean = atof(argv[i]);
                i++;
                s_span = atof(argv[i]);
            }
            else
            {
                fprintf(stderr, 
                    "Missing values:\n"
                    "  --match-saturation=<mean value>,<span value>\n"
                    "\n"
                );
                return -1;
            }
        }
        /* MATCH LIGHTNESS */
        else if(strncmp(arg, "--match-lightness", 17) == 0)
        {
            int n = 0;
            if(strncmp(arg, "--match-lightness=", n = 18) == 0 && arg_len > n)
            {
                // extract value
                char* val = &arg[n];
                float start = -1.f;
                float end = -1.f;
                sscanf(val, "%f-%f", &start, &end);

                l_span = clamp(end - start, 0.f, 1.f);
                l_mean = start + l_span / 2.f;

                // printf("parsed lightness range: %.1f%% - %.1f%%\n", start * 100.f, end * 100.f);
                // printf("derived mean and span: %.1f%% \u00b1%.1f%%\n", l_mean * 100.f, l_span * 50.f);
            }
            else
            {
                //fprintf(stderr, 
                printLineBreakingText(stderr, 
                    "Missing values -- option syntax:\n"
                    "  \033[96m--match-lightness=<start>-<end>\033[0m\n\n"
                    "<start>\n"
                    "  A value specifying the starting point within the lightness range [0..100%] to be matched (0 = unlit; 1 = fully lit)\n\n"
                    "<end>\n"
                    "  The end point within the lightness range [0..100%] to be matched.\n\n"
                    "Example:\n"
                    "  \033[96mcolerp \"#38a2ff\" --match-lightness=0%-40% -L40% -esqr\033[0m\n"
                    "  The above example aims to adjust the lightness of any input color that matches a lightness value in the range from 0 to 40 percent.\n"
                    "  This example will effectively set the minimum lightness of all input colors to 40 percent (cp. \033[96m-L40%\033[0m) without fading or blending (cp. \033[96m-esqr\033[0m).\n"
                    "\n"
                   , 0, 2, 0
                );
                return -1;
            }
        }
        /* POWER */
        else if(strncmp(arg, "--power", 7) == 0 || strncmp(arg, "-p", 2) == 0)
        {
            int n = 0;
            if(
                (strncmp(arg, "--power=", n = 8) == 0 && arg_len > n) || 
                (strncmp(arg, "-p=", n = 3) == 0 && arg_len > n) || 
                (strncmp(arg, "-p", n = 2) == 0 && arg_len > n && arg[n] != '='))
            {
                // extract value
                char* val = &arg[n];
                power_factor = clamp(atof(val), 0.f, 2.f);
            }
            else
            {
                fprintf(stderr, 
                    "Missing value:\n"
                    "  -p<percentage>, --power=<percentage> [0%%..500%%]\n"
                    "\n"
                );
                return -1;
            }
        }
        /* HUE TARGET */
        else if(strncmp(arg, "--hue", 5) == 0 || strncmp(arg, "-H", 2) == 0)
        {
            int n = 0;
            if(
                (strncmp(arg, "--hue=", n = 6) == 0 && arg_len > n) || 
                (strncmp(arg, "-H=", n = 3) == 0 && arg_len > n) || 
                (strncmp(arg, "-H", n = 2) == 0 && arg_len > n && arg[n] != '='))
            {
                // extract value
                char* val = &arg[n];
                if(val[0] == '+') {
                    h_change = atof(&val[1]);
                    h_target_set = 1;
                }
                else if(val[0] == '-') {
                    h_change = atof(val);
                    h_target_set = 2;
                }
                else {
                    h_change = atof(val);
                    h_target_set = 3;
                }

                mutated++;
            }
            else
            {
                fprintf(stderr, 
                    "Missing value:\n"
                    "  -H<value>, --hue=<value> [0..1|1..255|+0..1|-0..1]>\n"
                    "\n"
                );
                return -1;
            }
        }
        /* SATURATION TARGET */
        else if(strncmp(arg, "--saturation", 12) == 0 || strncmp(arg, "-S", 2) == 0)
        {
            int n = 0;
            if(
                (strncmp(arg, "--saturation=", n = 13) == 0 && arg_len > n) || 
                (strncmp(arg, "-S=", n = 3) == 0 && arg_len > n) || 
                (strncmp(arg, "-S", n = 2) == 0 && arg_len > n && arg[n] != '='))
            {
                // extract value
                char* val = &arg[n];
                if(val[0] == '+') {
                    s_change = atof(&val[1]);
                    s_target_set = 1;
                }
                else if(val[0] == '-') {
                    s_change = atof(val);
                    s_target_set = 2;
                }
                else {
                    s_change = atof(val);
                    s_target_set = 3;
                }

                mutated++;
            }
            else
            {
                fprintf(stderr, 
                    "Missing value:\n"
                    "  -S<value>, --saturation=<value> [0..1|1..255|+0..1|-0..1]>\n"
                    "\n"
                );
                return -1;
            }
        }
        /* LIGHTNESS TARGET */
        else if(strncmp(arg, "--lightness", 11) == 0 || strncmp(arg, "-L", 2) == 0)
        {
            int n = 0;
            if(
                (strncmp(arg, "--lightness=", n = 12) == 0 && arg_len > n) || 
                (strncmp(arg, "-L=", n = 3) == 0 && arg_len > n) || 
                (strncmp(arg, "-L", n = 2) == 0 && arg_len > n && arg[n] != '='))
            {
                // extract value
                char* val = &arg[n];
                if(val[0] == '+') {
                    l_change = atof(&val[1]);
                    l_target_set = 1;
                }
                else if(val[0] == '-') {
                    l_change = atof(val);
                    l_target_set = 2;
                }
                else {
                    l_change = atof(val);
                    l_target_set = 3;
                }

                mutated++;
            }
            else
            {
                fprintf(stderr, 
                    "Missing value:\n"
                    "  -L<value>, --lightness=<value> [0..1|1..255|+0..1|-0..1]>\n"
                    "\n"
                );
                return -1;
            }
        }
        /* EVALUATOR */
        else if(strncmp(arg, "--evaluator", 11) == 0 || strncmp(arg, "-e", 2) == 0)
        {
            int n = 0;
            if(
                (strncmp(arg, "--evaluator=", n = 12) == 0 && arg_len > n) || 
                (strncmp(arg, "-e=", n = 3) == 0 && arg_len > n) || 
                (strncmp(arg, "-e", n = 2) == 0 && arg_len > n && arg[n] != '='))
            {
                // extract value
                char* val = &arg[n];
                if(strcmp(val, "square") == 0 || strcmp(val, "sqr") == 0) {
                    eval = evalsquare;
                }
                else if(strcmp(val, "linear") == 0 || strcmp(val, "x1") == 0) {
                    eval = evallinear;
                }
                else if(strcmp(val, "quadratic") == 0 || strcmp(val, "x2") == 0) {
                    eval = evalquadratic;
                }
                else if(strcmp(val, "cubic") == 0 || strcmp(val, "x3") == 0) {
                    eval = evalcubic;
                }
                else if(strcmp(val, "quartic") == 0 || strcmp(val, "x4") == 0) {
                    eval = evalquartic;
                }
                else if(strcmp(val, "sinosoidal") == 0 || strcmp(val, "sin") == 0) {
                    eval = evalsinusoidal;
                }
                else
                {
                    fprintf(stderr, 
                        "Invalid value <%s>\n"
                        "  -e<function>, --evaluator=<function>\n\n"
                        "<function>\n"
                        "  The method of interpolating between input and output color.\n"
                        "  Choose from one of the following functions:\n"
                        "  Square:         as \033[96msquare\033[0m or \033[96msqr\033[0m; no blending, hard transition\n"
                        "  Linear:         as \033[96mlinear\033[0m or \033[96mx1\033[0m; linear blending from the mean to the edges\n"
                        "  Cuadratic:      as \033[96mquadratic\033[0m or \033[96mx2\033[0m; soft around the mean, hard at the edges\n"
                        "  Cubic:          as \033[96mcubic\033[0m or \033[96mx3\033[0m; softer around the mean, harder at the edges\n"
                        "  Quartic:        as \033[96mquartic\033[0m or \033[96mx4\033[0m; softest around the mean, hardest at the edges\n"
                        "  Sinosoidal:     as \033[96msinosoidal\033[0m or \033[96msin\033[0m; soft around the mean and the edges\n"
                        "\n"
                        , val
                    );
                    return -1;
                }
            }
            else
            {
                fprintf(stderr, 
                    "Missing value:\n"
                    "  -e<function>, --evaluator=<function>\n\n"
                    "<function>\n"
                    "  The method of interpolating between input and output color.\n"
                    "  Choose from one of the following functions:\n"
                    "  Square:         as \033[96msquare\033[0m or \033[96msqr\033[0m; no blending, hard transition\n"
                    "  Linear:         as \033[96mlinear\033[0m or \033[96mx1\033[0m; linear blending from the mean to the edges\n"
                    "  Quadratic:      as \033[96mquadratic\033[0m or \033[96mx2\033[0m; soft around the mean, hard at the edges\n"
                    "  Cubic:          as \033[96mcubic\033[0m or \033[96mx3\033[0m; softer around the mean, harder at the edges\n"
                    "  Quartic:        as \033[96mquartic\033[0m or \033[96mx4\033[0m; softest around the mean, hardest at the edges\n"
                    "  Sinosoidal:     as \033[96msinosoidal\033[0m or \033[96msin\033[0m; soft around the mean and the edges\n"
                    "\n"
                );
                return -1;
            }
        }
        /* DEBUG */
        else if(strcmp(arg, "--debug") == 0)
        {
            printLineBreakingText(stdout, 
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's "
                "standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to "
                "make a type specimen book. It has survived not only five centuries, but also the leap into electronic "
                "typesetting, remaining essentially unchanged.\nIt was popularised in the 1960s with the release of Letraset "
                "sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus "
                "PageMaker including versions of Lorem Ipsum."
               , 0, 2, 0
            );

            return 0;
        }
        // catch all options handled in the previous passes to have anything that remains be a color input string (else block)
        /* CATCH-ALL */
        else if(strncmp(arg, "-", 1) == 0)
        {
            // don't do anything here
        }
        /* COLOR INPUT STRING */
        else
        {
            inputcolor_t input_color = inputcolor_default;
            input_color.color_string = arg;
            int result = parseColorString(arg, NULL, &input_color);

            if(result < 0) {
                fprintf(stderr, 
                    "The given value \033[96m%s\033[00m could not be interpreted as a valid color value!\n", 
                    arg);
                return -2;
            }
            else
            {
                input_colors[num_actual_input_colors] = input_color;
                num_actual_input_colors++;
                source_color_set = 1;
            }
        }
    }

    // if source color was not specified as an argument read it from standard input
    if(source_color_set == 0)
    {
        if(scanf("%s", source_color_string) == 1)
        {
            inputcolor_t input_color = inputcolor_default;
            int result = parseColorString(source_color_string, &hsl_source, &input_color);

            if(result < 0) {
                fprintf(stderr, "The given value \033[91m%s\033[0m could not be interpreted as a valid color value!\n", 
                    source_color_string);
                return -2;
            }
        }
        else
        {
            fprintf(stderr, "Error: could not read from standard input!\n");
            return -3;
        }
    }

    for(int i = 0; i < num_actual_input_colors; i++)
    {
        hsl_source = input_colors[i].hsl;
        rgb_source = input_colors[i].rgb;

        // if(verbosity_mode == 1)
        //     printf("source hsl = (%.0f°, %.1f%%, %.1f%%)\n", hslsv_source.h * 360.f, hslsv_source.s * 100.f, hslsv_source.l * 100.f);

        // set hue target based on source
        if(h_target_set > 0)
        {
            // hue adjust relative
            if(h_target_set <= 2 ) {
                float change = clamp(h_change, 0.f, 1.f) * hsl_source.h;
                hsl_target.h = fmodx(hsl_source.h + change, 1.f);
            }
            // hue set absolute
            else {
                hsl_target.h = h_change;
                if(hsl_target.h > 1.f) hsl_target.h /= 255.f;
                hsl_target.h = clamp(hsl_target.h, 0.f, 1.f);
            }
        }
        else
            hsl_target.h = hsl_source.h;

        // set saturation target based on source
        if(s_target_set > 0)
        {
            // saturation adjust relative
            if(s_target_set <= 2 ) {
                float change = s_change * hsl_source.s;
                hsl_target.s = clamp(hsl_source.s + change, 0.f, 1.f);
            }
            // saturation set absolute
            else {
                hsl_target.s = s_change;
                if(hsl_target.s > 1.f) hsl_target.s /= 255.f;
                hsl_target.s = clamp(hsl_target.s, 0.f, 1.f);
            }
        }
        else
            hsl_target.s = hsl_source.s;

        // set lightness target based on source
        if(l_target_set > 0)
        {
            // lightness adjust relative
            if(l_target_set <= 2 ) {
                float change = l_change * hsl_source.l;
                hsl_target.l = clamp(hsl_source.l + change, 0.f, 1.f);
            }
            // lightness set absolute
            else {
                hsl_target.l = l_change;
                if(hsl_target.l > 1.f) hsl_target.l /= 255.f;
                hsl_target.l = clamp(hsl_target.l, 0.f, 1.f);
            }
        }
        else
            hsl_target.l = hsl_source.l;

        // determine mix factor
        float mix_factor = 1.f;
        mix_factor *= power_factor;
        if(mix_factor > 0.f && h_mean >= 0.f) mix_factor *= mixf(hsl_source.h, hsl_target.h, clamp(h_mean, 0.f, 1.f), clamp(h_span, 0.f, 2.f));
        else if(mix_factor > 0.f && s_mean >= 0.f) mix_factor *= mixf(hsl_source.s, hsl_target.s, clamp(s_mean, 0.f, 1.f), clamp(s_span, 0.f, 2.f));
        else if(mix_factor > 0.f && l_mean >= 0.f) mix_factor *= mixf(hsl_source.l, hsl_target.l, clamp(l_mean, 0.f, 1.f), clamp(l_span, 0.f, 2.f));
        //else mix_factor = 0.f;

        // if(verbosity_mode == 1)
        //     printf("color mixing factor: %.4f\n", mix_factor);

        // mix values
        hsl_result.h = mix(hsl_source.h, hsl_target.h, mix_factor);
        hsl_result.s = mix(hsl_source.s, hsl_target.s, mix_factor);
        hsl_result.l = mix(hsl_source.l, hsl_target.l, mix_factor);

        // convert from hsl to rgb
        hslToRgb(hsl_result, &rgb_result);

        // if(verbosity_mode == 1) {
        //     printf("result rgb = (%.0f, %.0f, %.0f)\n", r_result * 255.f, g_result * 255.f, b_result * 255.f);
        //     printf("result hsl = (%.0f°, %.1f%%, %.1f%%)\n", h_result * 360.f, s_result * 100.f, l_result * 100.f);
        // }

        int hex_result = 0;
        rgbToHex(rgb_result, &hex_result);

        // output colored blocks for visualization
        if(verbosity_mode == 1 || verbosity_mode == 2) {
            printf("\x1b[38;2;%.0f;%.0f;%.0fm\u25A0\u25A0\u25A0\u25A0\u25A0\x1b[0m", 
                rgb_source.r * 255.f, rgb_source.g * 255.f, rgb_source.b * 255.f);
            if(mutated > 0)
                printf(" -> \x1b[38;2;%.0f;%.0f;%.0fm\u25A0\u25A0\u25A0\u25A0\u25A0\x1b[0m", 
                    rgb_result.r * 255.f, rgb_result.g * 255.f, rgb_result.b * 255.f);
            printf("\n");
        }

        const int output_buffer_length = 256;
        char output_buffer[output_buffer_length];

        // output the final color value in hex format

        // use formally specified output format
        if(output_format_set == 1)
        {
            int reti = formatOutputColor(rgb_result, output_format_string, output_buffer, output_buffer_length);
            if(reti > 0) {
                if(verbosity_mode == 1)
                    printf("result color: ");
                printf("\033[96m%s\033[00m\n", output_buffer);
                if(reti == 2) {
                    fprintf(stderr, "\033[1;91mFailure: \033[0;91mThe size of the output buffer (%d characters) was exceeded. Please specify a shorter output format.\033[00m\n", 
                        output_buffer_length);
                }
            }
        }
        // use default output format
        else if(output_format_set == 0)
        {
            if(verbosity_mode == 1)
                printf("result hex: ");
            if(source_color_format == 1) printf("\033[96m#%06x\033[00m\n", hex_result);
            else printf("\033[96m%06x\033[00m\n", hex_result);
        }
    }

    // free resources
    if(input_colors != NULL)
    {
        for(int i = 0; i < num_actual_input_colors; i++)
        {
            inputcolor_t color = input_colors[i];
            if(color.num_components > 0 && color.components != NULL)
            {
                // free input color's components arrays
                free(color.components);
                color.components = NULL;
                color.num_components = 0;
            }
        }

        // free input colors array
        free(input_colors);
        input_colors = NULL;
    }

    // refresh();
    // endwin();

    return 0;
}
