/*
* utilities.c
* Utility and helper functions.
*
* Author:   Sebastian Zander
* Created:  2019-03-20
*/

#include <math.h>

#include "types.h"
#include "utilities.h"

evalfun eval;

float fmodx(float x, float y)
{
    if(x < 0.f) return -fmodf(-x, y) + y;
    else return fmodf(x, y);
}

float evalsquare(float x)
{
	if(x < -1.f || x >= 1.f)
		return 0.f;
	
	return 1.f;
}

float evallinear(float x)
{
	if(x < -1.f || x >= 1.f)
		return 0.f;
	
	return 1.f - fabs(x);
}

float evalquadratic(float x)
{
	if(x < -1.f || x >= 1.f)
		return 0.f;
	
	return 1.f - x * x;
}

float evalcubic(float x)
{
	if(x < -1.f || x >= 1.f)
		return 0.f;
	
	return 1.f - fabs(x * x * x);
}

float evalquartic(float x)
{
	if(x < -1.f || x >= 1.f)
		return 0.f;
	
	return 1.f - x * x * x * x;
}

float evalsinusoidal(float x)
{
	if(x < -1.f || x >= 1.f)
		return 0.f;
	
	return 0.5f + cos(x * M_PI) / 2.f;
}

float clamp(float value, float min, float max)
{
    if(value < min) return min;
    else if(value > max) return max;
    else return value;
}

float mixf(float source, float target, float mean, float span)
{
    // normalize source value
    float norm = (source - mean) / (span / 2.f);

    // evaluate and return mixing factor based on normalized source value
    return eval(norm);
}

float mix(float source, float target, float factor)
{
    // mix source with target based on evaluted mixing factor
    return source * (1.f - factor) + target * factor;
}