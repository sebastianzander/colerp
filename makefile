cc = gcc
ld = gcc

src_dir = src
inc_dir = include
lib_dir = lib
obj_dir = obj
dep_dir = dep

ccflags = -std=c11 -O3 -g -I$(inc_dir)
ldflags = -ltinfo -lncurses -lm

bin = colerp
obj_files = colerp.o converter.o parser.o utilities.o types.o
obj = $(patsubst %,$(obj_dir)/%,$(obj_files))

src_files = $(obj_files:%.o=%.c)
src = $(patsubst %,$(src_dir)/%,$(src_files))

inc = $(obj:%.o=%.h) defines.h types.h
dep = $(obj:%.o=%.d)

# vpath %.c src
# vpath %.h include
# vpath %.o obj

colerp: $(obj)
	$(ld) -o $(bin) $^ $(ldflags)

-include $(dep)

obj/colerp.o: src/colerp.c
	$(cc) $(ccflags) -o $@ -c $^ -lncurses

obj/converter.o: src/converter.c
	$(cc) $(ccflags) -o $@ -c $^

obj/parser.o: src/parser.c
	$(cc) $(ccflags) -o $@ -c $^

obj/types.o: src/types.c
	$(cc) $(ccflags) -o $@ -c $^

obj/utilities.o: src/utilities.c
	$(cc) $(ccflags) -o $@ -c $^

# $(obj_dir)/colerp.o: $(src_dir)/colerp.c
# 	$(cc) $(ccflags) -o $@ -c $^

# $(dep_dir)/%.d: $(src)
# 	$(cc) $(ccflags) $< -MM -MT $(@:.d=.o) >$@

.phony: clean
clean:
	rm -rf $(bin) $(obj_dir)/*.o

.phony: cleandep
cleandep:
	rm -rf $(dep)
